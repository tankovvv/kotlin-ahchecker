package ru.zoga_com.vkbot.commands

import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Database
import ru.zoga_com.vkbot.utils.Utils
import ru.zoga_com.vkbot.utils.db.Cats
import api.longpoll.bots.model.events.messages.MessageNew
import java.sql.ResultSet

object CatsCommand: Command() {
    override fun onCommand(args: Array<String>, message: MessageNew) {
        if(args.size == 1) {
            Utils().sendMessage("Статистика котят:\n\nУбито котят в беседе: ${Cats.getCats("${message.getMessage().peerId}").getLong("killed")}\nВсего котят убито: ${Cats.getCats("ALL").getLong("killed")}\n\nКотята умирают, если кто-то матерится.", message.getMessage().getPeerId())
        }
    }
}