package ru.zoga_com.vkbot.commands

import api.longpoll.bots.LongPollBot
import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Utils
import api.longpoll.bots.model.events.messages.MessageNew

object TestCommand: Command() {
    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            Utils().sendMessage("Количество потоков: ${Runtime.getRuntime().availableProcessors()}\nИспользуемая память: ${(Runtime.getRuntime().totalMemory() / 1024 / 1024) - (Runtime.getRuntime().freeMemory() / 1024 / 1024)} MB (максимум ${Runtime.getRuntime().totalMemory() / 1024 / 1024} MB)\nGC размер: ${Runtime.getRuntime().maxMemory() / 1024 / 1024} MB", message.getMessage().getPeerId())
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName}: ${e.message} (Повторите попытку снова)", message.getMessage().getPeerId())
            e.printStackTrace()
        }
    }
}