package ru.zoga_com.vkbot.commands

import kotlin.text.Regex
import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.types.User
import ru.zoga_com.vkbot.utils.Utils
import ru.zoga_com.vkbot.utils.Database
import ru.zoga_com.vkbot.utils.db.Users
import api.longpoll.bots.model.events.messages.MessageNew
import java.sql.ResultSet

object ProfileCommand: Command() {
    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            when(args.size) { 
                1 -> {
                    val result: ResultSet = Users.getUser(message.getMessage().getFromId(), "cock_length,last_cock,longest_cock")
                    if(Utils().createUserIfNotExists(result, message)) {
                        val user: User = User(message.getMessage().getFromId().toLong())
                        Utils().sendMessage("Профиль @id${user.vk_id} (${user.nick}):\n\nДлина кока: ${user.cock_length} см\nСамый большой кок: ${user.longest_cock} см\nПоследнее увеличение: ${Utils().formatTime(user.last_cock)}\n\n✂️ Шанс отвала кока: ${user.cock_length.toDouble() / 100}%\n\nПрофиль создан: ${Utils().formatTime(user.register_date)}", message.getMessage().getPeerId())
                    } else { Utils().sendMessage("Ваш профиль не найден, поэтому сейчас он будет создан.\n\nВыполните команду снова.", message.getMessage().getPeerId()) }
                }
                2 -> {
                    if(args[1].toString().startsWith("[id")) { 
                        val id: Long = (args[1].split("|")[0].replace("[", "").replace("id", "")).toLong()
                        val result: ResultSet = Users.getUser(id.toInt(), "cock_length,last_cock,longest_cock")
                        if(Utils().checkUserExists(result)) {
                            val user: User = User(id)
                            Utils().sendMessage("Профиль @id${user.vk_id} (${user.nick}):\n\nДлина кока: ${user.cock_length} см\nСамый большой кок: ${user.longest_cock} см\nПоследнее увеличение: ${Utils().formatTime(user.last_cock)}\n\n✂️ Шанс отвала кока: ${user.cock_length.toDouble() / 100}%\n\nПрофиль создан: ${Utils().formatTime(user.register_date)}", message.getMessage().getPeerId())
                        } else { Utils().sendMessage("Данный профиль не найден.", message.getMessage().getPeerId()) }
                    }
                }
            }
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName}: ${e.message} (Повторите попытку снова)", message.getMessage().getPeerId())
            e.printStackTrace()
        }
    }
}
