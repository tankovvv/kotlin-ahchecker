package ru.zoga_com.vkbot.commands

import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Utils
import api.longpoll.bots.model.events.messages.MessageNew

object HelpCommand: Command() {
    override fun onCommand(args: Array<String>, message: MessageNew) {
        if(args.size == 1) {
            Utils().sendMessage("Команды:\n/sb <ник>\n/sw <ник>\n/bw <ник>\n\nОстальные команды:\nvk.com/@hypixel_bot-commands", message.getMessage().getPeerId())
        }
    }
}