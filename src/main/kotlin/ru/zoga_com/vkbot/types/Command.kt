package ru.zoga_com.vkbot.types

import api.longpoll.bots.model.events.messages.MessageNew

abstract class Command {
    abstract fun onCommand(args: Array<String>, message: MessageNew)
}