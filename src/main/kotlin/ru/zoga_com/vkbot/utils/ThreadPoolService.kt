package ru.zoga_com.vkbot.utils

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object ThreadPoolService {
   val pool: ExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
   
   fun createTask(task: Runnable) {
      pool.execute(task)
   }
}
