package ru.zoga_com.vkbot.utils

import ru.zoga_com.vkbot.commands.ChainsCommand
import com.ppolivka.namegenerator.impl.KatzBackoffGenerator
import com.ppolivka.namegenerator.Generator
import java.io.File
import java.io.BufferedReader
import java.io.InputStreamReader

object Chains {
    val chainSet: MutableSet<String> = mutableSetOf()
    var generator: Generator? = null

    fun fillSet() {
        val file = this::class.java.getResourceAsStream("/chains.txt")
        var count: Int = 0
        BufferedReader(InputStreamReader(file)).use { stream -> stream.lines().forEach { 
            chainSet.add(it)
            count++ 
        } }
        generator = KatzBackoffGenerator(chainSet, 2, 0.001f)
        println("MutableSet заполнен. Добавлено ${count} записей.")
    }

    fun generate(): String {
        return generator!!.generate()
    }
}
