package ru.zoga_com.vkbot.utils

import api.longpoll.bots.LongPollBot
import api.longpoll.bots.model.events.messages.MessageNew
import api.longpoll.bots.model.objects.additional.buttons.Button
import api.longpoll.bots.model.objects.additional.buttons.CallbackButton
import api.longpoll.bots.model.objects.additional.Keyboard
import api.longpoll.bots.methods.impl.users.Get
import api.longpoll.bots.methods.impl.messages.Send
import java.io.File
import java.io.InputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.net.HttpURLConnection
import java.nio.charset.Charset
import java.sql.ResultSet
import org.json.JSONObject
import org.json.JSONArray
import org.apache.commons.io.IOUtils
import java.util.Calendar
import java.util.concurrent.CompletableFuture
import java.util.regex.Pattern
import java.util.Collections
import ru.zoga_com.vkbot.types.User
import ru.zoga_com.vkbot.utils.db.Cats
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class Utils: LongPollBot() {
    val apiKey: String? = YamlParser().getConfigValue("key")

    override fun getAccessToken(): String {
        return YamlParser().getConfigValue("token")
    }

    fun sendMessage(message: String, peer: Int) {
        var messageFormed: String = message
        messageFormed.replace(apiKey.toString(), "СКРЫТО")
        messageFormed.replace(getAccessToken(), "СКРЫТО")
        vk.messages.send().setPeerId(peer).setMessage(messageFormed).addParam("disable_mentions", 1).executeAsync()
    }

    fun sendDeletingMessage(message: String, peer: Int) {
        var messageFormed: String = message
        messageFormed.replace(apiKey.toString(), "СКРЫТО")
        messageFormed.replace(getAccessToken(), "СКРЫТО")
        val deleteMessageSign: JsonObject = JsonObject()
        deleteMessageSign.addProperty("deleteMessage", true)
        val button: Button = CallbackButton(Button.Color.NEGATIVE, CallbackButton.Action("Удалить", deleteMessageSign))
        val keyboard: Keyboard = Keyboard(Collections.singletonList(Collections.singletonList(button))).setInline(true)
        vk.messages.send().setPeerId(peer).setMessage(messageFormed).addParam("disable_mentions", 1).setKeyboard(keyboard).executeAsync()
    }

    fun sendMessageWithPhoto(message: String, peer: Int, photo: File) {
        var messageFormed: String = message
        messageFormed.replace(apiKey.toString(), "СКРЫТО")
        messageFormed.replace(getAccessToken(), "СКРЫТО")
        vk.messages.send().setPeerId(peer).setMessage(messageFormed).addPhoto(photo).addParam("disable_mentions", 1).executeAsync()
    }

    fun getNameById(id: Int): String {
        val res: Get.ResponseBody = vk.users.get().setUserIds(id.toString()).execute()
        return res.response.get(0).firstName
    }

    fun getIdByShort(short: String): Long {
        val res: Get.ResponseBody = vk.users.get().setUserIds(short).execute()
        return res.response.get(0).id.toLong()
    }

    fun getBazaarItem(json: String, path: String): String {
        val jsonObject: JSONObject = JSONObject(json)
        val jsonPath: Array<String> = path.split(".").toTypedArray()
        return when(jsonObject.getJSONObject(jsonPath[0]).get(jsonPath[1])) {
            null -> "null"
            else -> jsonObject.getJSONObject(jsonPath[0]).get(jsonPath[1]).toString() 
        }
    }

    fun getPlayerProfile(player: String): JSONObject {
        val jsonObj: JSONObject = JSONObject(getJson("https://api.slothpixel.me/api/skyblock/profile/" + player))
        return jsonObj.getJSONObject("members").getJSONObject(getUUID(player))
    }

    fun getJson(url: String): String {
        return IOUtils.toString(URL(url), Charset.forName("UTF-8"))
    }

    fun getUUID(player: String): String {
        return JSONObject(getJson("https://api.mojang.com/users/profiles/minecraft/" + player)).getString("id")
    }

    fun getNick(uuid: String): String {
        return JSONObject(getJson("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid)).getString("name")
    }

    fun getArmor(player: String): HashMap<String, String> {
        val armor: JSONArray = getPlayerProfile(player).getJSONArray("armor")
        val mapArmor: HashMap<String, String> = hashMapOf()
        mapArmor.put("helmet", removeColorCodes(armor.getJSONObject(3).getString("name")))
        mapArmor.put("helmetRarity", removeColorCodes(armor.getJSONObject(3).getString("rarity")))
        mapArmor.put("chestplate", removeColorCodes(armor.getJSONObject(2).getString("name")))
        mapArmor.put("chestplateRarity", removeColorCodes(armor.getJSONObject(2).getString("rarity")))
        mapArmor.put("leggings", removeColorCodes(armor.getJSONObject(1).getString("name")))
        mapArmor.put("leggingsRarity", removeColorCodes(armor.getJSONObject(1).getString("rarity")))
        mapArmor.put("boots", removeColorCodes(armor.getJSONObject(0).getString("name")))
        mapArmor.put("bootsRarity", removeColorCodes(armor.getJSONObject(0).getString("rarity")))
        return mapArmor
    }

    fun removeColorCodes(str: String): String {
        var out: String = str
        val codes: List<String> = listOf("§0", "§1", "§2", "§3", "§4", "§5", "§6", "§7", "§8", "§9", "§l", "§o", "§r", "§n", "§m", "§k", "§a", "§b", "§c", "§d", "§e", "§f")
        for(s: String in codes) {
            out = out.replace(s, "")
        }
        return out
    }

    fun createUserIfNotExists(result: ResultSet, info: MessageNew): Boolean {
        when(result.next()) {
            false -> {
                Database().insert("INSERT INTO users(vk_id,last_cock,cock_length,nick,longest_cock,register_date) VALUES(${info.getMessage().getFromId()},0,0,\"false\",0,${System.currentTimeMillis()});")
                return false
            }
            true -> return true
        }
    }

    fun checkUserExists(result: ResultSet): Boolean {
        return result.next()
    }

    fun formatTime(time: Long): String {
        val calendar: Calendar = Calendar.getInstance()
        calendar.setTimeInMillis(time)
        var day: String = (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        var month: String = ((calendar.get(Calendar.MONTH) + 1)).toString()
        var hour: String = (calendar.get(Calendar.HOUR_OF_DAY)).toString()
        var minute: String = (calendar.get(Calendar.MINUTE)).toString()
        val realTime: Calendar = Calendar.getInstance()
        realTime.setTimeInMillis(System.currentTimeMillis())
        if(calendar.get(Calendar.MINUTE) < 10) {
            minute = "0" + minute
        }
        if(calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            hour = "0" + hour
        }
        if(calendar.get(Calendar.MONTH) < 9) {
            month = "0" + month
        }
	if(calendar.get(Calendar.DAY_OF_MONTH) < 9) {
            day = "0" + day
        }
        return when(calendar.get(Calendar.DAY_OF_MONTH)) {
            (realTime.get(Calendar.DAY_OF_MONTH) - 1) -> "Вчера, в ${hour}:${minute}"
            realTime.get(Calendar.DAY_OF_MONTH) -> "Сегодня, в ${hour}:${minute}"
            (realTime.get(Calendar.DAY_OF_MONTH) + 1) -> "Завтра, в ${hour}:${minute}"
            else -> "${day}.${month}.${calendar.get(Calendar.YEAR)} в ${hour}:${minute}"
        }
    }

    fun formatTimeInHours(time: Long): String {
        val calendar: Calendar = Calendar.getInstance()
        calendar.setTimeInMillis(time)
        var hour: String = (calendar.get(Calendar.HOUR_OF_DAY)).toString()
        var minute: String = (calendar.get(Calendar.MINUTE)).toString()
        var sec: String = (calendar.get(Calendar.SECOND)).toString()
        if(calendar.get(Calendar.SECOND) < 10) {
            sec = "0" + sec
        }
        if(calendar.get(Calendar.MINUTE) < 10) {
            minute = "0" + minute
        }
        if(calendar.get(Calendar.HOUR) < 10) {
            hour = "0" + hour
        }
        return "${hour}:${minute}:${sec}"
    }

    fun checkPlayerExists(name: String): Boolean {
        val connection: HttpURLConnection = URL("https://api.mojang.com/users/profiles/minecraft/${name}").openConnection() as HttpURLConnection
        connection.setRequestMethod("GET")
        connection.connect()
        val code: Int = connection.getResponseCode()
        connection.disconnect()
        return when(code) {
            204 -> false
            400 -> false
            else -> true
        }
    }

    fun calculateNetworkLevel(exp: Double): Double {
        return (Math.sqrt((2 * exp) + 30625) / 50) - 2.5
    }

    fun triggerKillCat(message: MessageNew) {
        if(Pattern.compile("(?<=(^|[^а-я]))((у|[нз]а|(хитро|не)?вз?[ыьъ]|с[ьъ]|(и|ра)[зс]ъ?|(о[тб]|под)[ьъ]?|(.\\B)+?[оаеи])?-?([её]б(?!о[рй])|и[пб][ае][тц]).*?|(н[иеа]|([дп]|верт)о|ра[зс]|з?а|с(ме)?|о(т|дно)?|апч)?-?ху([яйиеёю]|ли(?!ган)).*?|(в[зы]|(три|два|четыре)жды|(н|сук)а)?-?бл(я(?!(х|ш[кн]|мб)[ауеыио]).*?|[еэ][дт]ь?)|(ра[сз]|[зн]а|[со]|вы?|п(ере|р[оие]|од)|и[зс]ъ?|[ао]т)?п[иеё]зд.*?|(за)?п[ие]д[аое]?р(ну.*?|[оа]м|(ас)?(и(ли)?[нщктл]ь?)?|(о(ч[еи])?|ас)?к(ой)|юг)[ауеы]?|манд([ауеыи](л(и[сзщ])?[ауеиы])?|ой|[ао]вошь?(е?к[ауе])?|юк(ов|[ауи])?)|муд([яаио].*?|е?н([ьюия]|ей))|мля([тд]ь)?|лять|([нз]а|по)х|м[ао]л[ао]фь([яию]|[еёо]й))(?=($|[^а-я]))").matcher(message.getMessage().text).find()) {
            val result: ResultSet = Cats.getCats("ALL")
            val cats: Long = result.getLong("killed")
            Database().insert("UPDATE cats SET killed = ${cats + 1} WHERE scope = \"ALL\";")

            Database().createPeerCatsCounterIfNotExists(message.getMessage().peerId.toLong())
            val resultLocal: ResultSet = Cats.getCats("${message.getMessage().peerId}")
            val catsLocal: Long = resultLocal.getLong("killed")
            Database().insert("UPDATE cats SET killed = ${catsLocal + 1} WHERE scope = \"${message.getMessage().peerId}\";")
        }
    }

    fun getPlayerAuctions(uuid: String): JSONObject {
        return JSONObject(getJson("https://api.hypixel.net/skyblock/auction?key=${apiKey}&player=${uuid}"))
    }

    fun findDeclension(number: Long, cases: Array<String>): String {
        var value: Long = Math.abs(number) % 100;
        var miniValue: Long = value % 10;
        if (value > 10 && value < 20) return cases[2];
        if (miniValue > 1 && miniValue < 5) return cases[1];
        if (miniValue == (1).toLong()) return cases[0];
        return cases[2];
    }
}
